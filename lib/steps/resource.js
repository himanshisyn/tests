"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../util");
const globs = require("globby");
const path_1 = require("path");
function resource(compiler, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const { cwd, resources } = compiler.options;
        if (!resources.length) {
            return next();
        }
        const step = compiler.log.step('Bundling Resources...');
        let count = 0;
        // workaround for https://github.com/sindresorhus/globby/issues/127
        // and https://github.com/mrmlnc/fast-glob#pattern-syntax
        const resourcesWithForwardSlashes = resources.map((r) => r.replace(/\\/g, '/'));
        yield (0, util_1.each)(globs(resourcesWithForwardSlashes, { cwd, onlyFiles: true }), (file) => __awaiter(this, void 0, void 0, function* () {
            count++;
            step.log(`Including file: ${file}`);
            yield compiler.addResource((0, path_1.resolve)(cwd, file));
        }));
        step.log(`Included ${count} file(s)`);
        return next();
    });
}
exports.default = resource;
