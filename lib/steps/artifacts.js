"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const fs = require("fs");
const util_1 = require("util");
const util_2 = require("../util");
const mkdirpAsync = require("mkdirp");
const unlinkAsync = (0, util_1.promisify)(fs.unlink), readdirAsync = (0, util_1.promisify)(fs.readdir);
function readDirAsync(dir) {
    return readdirAsync(dir).then((paths) => {
        return Promise.all(paths.map((file) => {
            const path = (0, path_1.join)(dir, file);
            return (0, util_2.isDirectoryAsync)(path).then((x) => (x ? readDirAsync(path) : path));
        })).then((result) => {
            return [].concat(...result);
        });
    });
}
function maybeReadFileContentsAsync(file) {
    return (0, util_2.readFileAsync)(file, 'utf-8').catch((e) => {
        if (e.code === 'ENOENT') {
            return '';
        }
        throw e;
    });
}
/**
 * The artifacts step is where source patches are committed, or written as "artifacts"
 * Steps:
 *  - A temporary directory is created in the downloaded source
 *  - On start, any files in that directory are restored into the source tree
 *  - After the patch functions have run, the temporary directory is emptied
 *  - Original versions of sources to be patched are written to the temporary directory
 *  - Finally, The patched files are written into source.
 */
function artifacts(compiler, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const { src } = compiler;
        const temp = (0, path_1.join)(src, 'nexe');
        yield mkdirpAsync(temp);
        const tmpFiles = yield readDirAsync(temp);
        yield Promise.all(tmpFiles.map((path) => __awaiter(this, void 0, void 0, function* () {
            return compiler.writeFileAsync(path.replace(temp, ''), yield (0, util_2.readFileAsync)(path, 'utf-8'));
        })));
        yield next();
        yield Promise.all(tmpFiles.map((x) => unlinkAsync(x)));
        return Promise.all(compiler.files.map((file) => __awaiter(this, void 0, void 0, function* () {
            const sourceFile = (0, path_1.join)(src, file.filename);
            const tempFile = (0, path_1.join)(temp, file.filename);
            const fileContents = yield maybeReadFileContentsAsync(sourceFile);
            yield mkdirpAsync((0, path_1.dirname)(tempFile));
            yield (0, util_2.writeFileAsync)(tempFile, fileContents);
            yield compiler.writeFileAsync(file.filename, file.contents);
        })));
    });
}
exports.default = artifacts;
