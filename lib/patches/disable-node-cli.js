"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../util");
function disableNodeCli(compiler, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (compiler.options.enableNodeCli) {
            return next();
        }
        if ((0, util_1.semverGt)(compiler.target.version, '11.6.0')) {
            yield compiler.replaceInFileAsync('src/node.cc', /(?<!int )ProcessGlobalArgs\(argv[^;]*;/gm, '0;/*$&*/');
        }
        else if ((0, util_1.semverGt)(compiler.target.version, '10.9')) {
            yield compiler.replaceInFileAsync('src/node.cc', /(?<!void )ProcessArgv\(argv/g, '//$&');
        }
        else if ((0, util_1.semverGt)(compiler.target.version, '9.999')) {
            yield compiler.replaceInFileAsync('src/node.cc', 'int i = 1; i < v8_argc; i++', 'int i = v8_argc; i < v8_argc; i++');
            let matches = 0;
            yield compiler.replaceInFileAsync('src/node.cc', /v8_argc > 1/g, (match) => {
                if (matches++) {
                    return 'false';
                }
                return match;
            });
        }
        else {
            const nodeccMarker = 'argv[index][0] ==';
            yield compiler.replaceInFileAsync('src/node.cc', `${nodeccMarker} '-'`, 
            // allow NODE_OPTIONS, introduced in 8.0
            (0, util_1.semverGt)(compiler.target.version, '7.99')
                ? `(${nodeccMarker} (is_env ? '-' : ']'))`
                : `(${nodeccMarker} ']')`);
        }
        return next();
    });
}
exports.default = disableNodeCli;
