"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const util_1 = require("../util");
function default_1(compiler, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const { snapshot, warmup, cwd } = compiler.options;
        if (!snapshot) {
            return next();
        }
        const variablePrefix = (0, util_1.semverGt)(compiler.target.version, '11.0.0') ? 'v8_' : '';
        yield compiler.replaceInFileAsync(compiler.configureScript, 'def configure_v8(o):', `def configure_v8(o):\n  o['variables']['${variablePrefix}embed_script'] = r'${(0, path_1.resolve)(cwd, snapshot)}'\n  o['variables']['${variablePrefix}warmup_script'] = r'${(0, path_1.resolve)(cwd, warmup || snapshot)}'`);
        return next();
    });
}
exports.default = default_1;
