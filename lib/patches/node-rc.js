"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function nodeRc(compiler, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const options = compiler.options.rc;
        if (!options) {
            return next();
        }
        const file = yield compiler.readFileAsync('src/res/node.rc');
        Object.keys(options).forEach((key) => {
            let value = options[key];
            const isVar = /^[A-Z_]+$/.test(value);
            value = isVar ? value : `"${value}"`;
            file.contents = file.contents
                .toString()
                .replace(new RegExp(`VALUE "${key}",.*`), `VALUE "${key}", ${value}`);
        });
        ['PRODUCTVERSION', 'FILEVERSION'].forEach((x) => {
            if (options[x]) {
                file.contents = file.contents
                    .toString()
                    .replace(new RegExp(x + ' .*$', 'm'), `${x} ${options[x]}`);
            }
        });
        return next();
    });
}
exports.default = nodeRc;
